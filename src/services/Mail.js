const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    host: "smtp-mail.outlook.com",
    port: 587,
    secure: false,
    auth: {
        user: "e-commercetp@outlook.fr",
        pass: "Livecampus2023*",
    },
    tls: {
        ciphers: 'SSLv3'
    }
});

exports.sendMailOrderOK = async (userEmail, product) => {
  // Configurer les options du mail
  let mailOptions = {
    from: "e-commercetp@outlook.fr",
    to: userEmail,
    subject: "Comfirmation de commande",
    text:
      "Bonjour, votre commande a bien été prise en compte. Merci de votre confiance. Pour le produit : \n" + product + "\n Cordialement, l'équipe E-commerce"
  };

  // Envoyer l'email
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      console.log("Error sending email : " + error);
    } else {
      console.log("Email sent successfully : " + info.response);
    }
  });
};