// Importer le package HTTP de Node.js pour avoir les outils pour créer le serveur
const http = require("http");

const db = require("./db/db");
// Importer l'application app.js
const { app } = require("./app");

// Importer le package pour utiliser les variables d'environnement
require("dotenv").config();

// Paramètrage du port avec la méthode set d'Express
app.set("port", process.env.PORT);

// La méthode createServer() prend en argument
// La fonction qui sera appelé à chaque requête reçu par le
// Serveur ici les fonctions seront dans app.js
const server = http.createServer(app);

const { createClient } = require("redis");
const mailService = require("./services/mail");
const logModel = require("./models/Log");
const moment = require("moment-timezone");

const socketIo = require("socket.io")
const io = socketIo(server,{
    cors: {
        origin: "*",
        methods: ["GET", "POST"],
    },
});

global.io = io;

// Le serveur écoute les requêtes sur le port
server.listen(process.env.PORT, async function () {
    console.log("Listening on port %s...", process.env.PORT);

    const client = await createClient({
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
    }).on("error", (err) => console.log("Redis Client Error", err)).connect();

    const listener = async (message, channel) => {
        console.log(message);
        const mess = JSON.parse(message);
        await logModel.create({date: moment().tz("Europe/Paris").format("DD-MM-YYYY HH:mm"), message: message, channel: channel});
        //mailService.sendMailOrderOK(mess.email, mess.product);
    };

    await client.subscribe('commande', listener);
});