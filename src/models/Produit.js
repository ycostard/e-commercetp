const db = require("../db/db");

class Produit {
  static create(newProduit) {
    return new Promise((resolve, reject) => {
      db.run(
        `INSERT INTO PRODUIT (name, description, price, quantity) VALUES (?, ?, ?, ?)`,
        [newProduit.name, newProduit.description, newProduit.price, newProduit.quantity],
        function (err) {
          if (err) {
            reject(err);
          } else {
            resolve(this.lastID);
          }
        }
      );
    });
  }
  
  static getAll() {
    return new Promise((resolve, reject) => {
      db.all(`SELECT * FROM PRODUIT`, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }

  static getById(id) {
    return new Promise((resolve, reject) => {
      db.get(`SELECT * FROM PRODUIT WHERE id = ?`, [id], (err, row) => {
        if (err) {
          reject(err);
        } else {
          resolve(row);
        }
      });
    });
  }

  static delete(id) {
    return new Promise((resolve, reject) => {
      db.run(`DELETE FROM PRODUIT WHERE id = ?`, [id], (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(null);
        }
      });
    });
  }

  static updateQuantity(id, quantity) {
    return new Promise((resolve, reject) => {
      db.run(`UPDATE PRODUIT SET quantity = ? WHERE id = ?`, [quantity, id], (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(null);
        }
      });
    });
  }
}

module.exports = Produit;