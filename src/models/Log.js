const db = require("../db/db");

class Log {
  static create(newLog) {
    return new Promise((resolve, reject) => {
      db.run(
        `INSERT INTO LOG (date, message, channel) VALUES (?, ?, ?)`,
        [newLog.date, newLog.message,newLog.channel],
        function (err) {
          if (err) {
            reject(err);
          } else {
            resolve(this.lastID);
          }
        }
      );
    });
  }
  
  static getAll() {
    return new Promise((resolve, reject) => {
      db.all(`SELECT * FROM LOG`, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }
  
  static getByChannel(channel) {
    return new Promise((resolve, reject) => {
      db.all(`SELECT * FROM LOG WHERE channel = ?`,[channel], (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }
}

module.exports = Log;