const db = require("../db/db");

class Commande {
  static create(newCommande) {
    return new Promise((resolve, reject) => {
      db.run(
        `INSERT INTO COMMANDE (name, productId) VALUES (?, ?)`,
        [newCommande.name, newCommande.productId],
        function (err) {
          if (err) {
            reject(err);
          } else {
            resolve(this.lastID);
          }
        }
      );
    });
  }
  
  static getAll() {
    return new Promise((resolve, reject) => {
      db.all(`SELECT * FROM COMMANDE`, (err, rows) => {
        if (err) {
          reject(err);
        } else {
          resolve(rows);
        }
      });
    });
  }

  static getById(id) {
    return new Promise((resolve, reject) => {
      db.get(`SELECT * FROM COMMANDE WHERE id = ?`, [id], (err, row) => {
        if (err) {
          reject(err);
        } else {
          resolve(row);
        }
      });
    });
  }

  static delete(id) {
    return new Promise((resolve, reject) => {
      db.run(`DELETE FROM COMMANDE WHERE id = ?`, [id], (err) => {
        if (err) {
          reject(err);
        } else {
          resolve(null);
        }
      });
    });
  }
}

module.exports = Commande;