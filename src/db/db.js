const sqlite3 = require("sqlite3");

const db = new sqlite3.Database('./database/database.db', (err) => {
  if (err) throw err;
  console.log("Database start");
});

db.run(`CREATE TABLE IF NOT EXISTS COMMANDE (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, productId INTEGER)`);
db.run(`CREATE TABLE IF NOT EXISTS PRODUIT (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, description TEXT, price INTEGER, quantity INTEGER)`);
db.run(`CREATE TABLE IF NOT EXISTS LOG (id INTEGER PRIMARY KEY AUTOINCREMENT, date TEXT, message TEXT, channel TEXT)`);


module.exports = db;