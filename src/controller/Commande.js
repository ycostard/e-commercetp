const commandeModel = require("../models/Commande");
const productModel = require("../models/Produit");
const { createClient } = require("redis");

class CommandeController {
  static async getAll(req, res) {
    try {
      const commandes = await commandeModel.getAll();
      res.json(commandes);
    } catch (error) {
      res.status(500).json(error);
    }
  }

  static async create(req, res) {
    try {
      const { name, productId, email } = req.body;

      const product = await productModel.getById(productId);

      io.emit("commande",{ message: "Vérification de l'existance du produit", progress: 10});
      // ATTENDRE 5 secondes avant de passer a la suite
      await new Promise((resolve) => setTimeout(resolve, 5000));
      if (!product) {
        return res.status(404).json({ message: "Produit non trouvé" });
      }
      io.emit("commande", {message: "Vérification de l'existance du produit terminée avec succès", progress: 30});
      await new Promise((resolve) => setTimeout(resolve, 5000));
      io.emit("commande", {message: "Vérification de la disponibilité du produit", progress: 40});
      await new Promise((resolve) => setTimeout(resolve, 5000));
      if (product.quantity <= 0) {
        return res.status(404).json({ message: "Produit plus disponible" });
      }
      await new Promise((resolve) => setTimeout(resolve, 5000));
      io.emit("commande", {message: "Vérification de la disponibilité du produit terminée avec succès", progress: 70});
      await new Promise((resolve) => setTimeout(resolve, 5000));
      const newCommande = { name, productId };

      await productModel.updateQuantity(product.id, product.quantity - 1);

      await commandeModel.create(newCommande);
      io.emit("commande", {message: "Commande validée", progress: 100});

      //Envoi a redis de la commande pour le traitement
      const client = await createClient({
        host: process.env.REDIS_HOST,
        port: process.env.REDIS_PORT,
      })
        .on("error", (err) => console.log("Redis Client Error", err))
        .connect();

      let commandePublish;

      const reply = await client.publish("commande",JSON.stringify((commandePublish = {email: email,product: product.name,})));
      console.log(reply);
      client.quit();

      res.json({ message: "Commande validé" });
    } catch (error) {
      console.log(error);
      res.status(500).json(error);
    }
  }

  static async getById(req, res) {
    try {
      const { id } = req.params;

      const commande = await commandeModel.getById(id);
      res.json(commande);
    } catch (error) {
      res.status(500).json(error);
    }
  }

  static async delete(req, res) {
    try {
      const { id } = req.params;

      await commandeModel.delete(id);
      res.json({ message: "Commande supprimé !" });
    } catch (error) {
      res.status(500).json(error);
    }
  }
}

module.exports = CommandeController;
