const produitModel = require("../models/Produit");

class ProduitController {
    static async getAll(req, res) {
      try {
        const produits = await produitModel.getAll();
        res.json(produits);
      } catch (error) {
        res.status(500).json(error);
      }
    }
  
    static async create(req, res) {
      try {
        const { name, description, price, quantity } = req.body;
  
        const newProduit = { name, description, price, quantity };
        const id = await produitModel.create(newProduit);
        res.json({ id });
      } catch (error) {
        console.log(error);
        res.status(500).json(error);
      }
    }
  
    static async getById(req, res) {
      try {
        const { id } = req.params;
  
        const book = await produitModel.getById(id);
        res.json(book);
      } catch (error) {
        res.status(500).json(error);
      }
    }
  
    static async delete(req, res) {
      try {
        const { id } = req.params;
        
        await produitModel.delete(id);
        res.json({ message: "Produit supprimé !" });
      } catch (error) {
        res.status(500).json(error);
      }
    }
  }

  module.exports = ProduitController;