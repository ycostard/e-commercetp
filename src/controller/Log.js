const logModel = require("../models/Log");

class LogController {
  static async getAll(req, res) {
    try {
        const { channel } = req.query;
        let logs;

        if(channel == undefined){
            logs = await logModel.getAll();
            res.json(logs);
        }else{
            logs = await logModel.getByChannel(channel);
            res.json(logs);
        }
    } catch (error) {
      res.status(500).json(error);
    }
  }
}

module.exports = LogController;
