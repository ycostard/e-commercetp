const express = require("express");
const router = express.Router();
const produitController = require("../controller/Produit");

router.get("/produits", produitController.getAll);
router.get('/produits/:id', produitController.getById);
router.post('/produits', produitController.create);
router.delete('/produits/:id', produitController.delete);

module.exports =router;