const express = require("express");
const router = express.Router();
const commandeController = require("../controller/Commande");

router.get("/commandes", commandeController.getAll);
router.get('/commandes/:id', commandeController.getById);
router.post('/commandes', commandeController.create);
router.delete('/commandes/:id', commandeController.delete);
 
module.exports =router;