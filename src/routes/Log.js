const express = require("express");
const router = express.Router();
const logController = require("../controller/Log");

router.get("/logs",logController.getAll);
 
module.exports =router;