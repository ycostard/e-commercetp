// Importation d'Express
const express = require("express");
const cors = require("cors");
const session = require('express-session');
const Keycloak = require('keycloak-connect');
// Importation des routes
const commandeRoutes = require("./routes/Commande");
const produitRoutes = require("./routes/Produit");
const logRoutes = require("./routes/Log");
// Pour créer l'application express
const app = express();

// Importation de body-parser
const bodyParser = require("body-parser");

// Logger les requêtes et les réponses
app.use(cors());

// Transformer le corps en json objet javascript utilisable
app.use(bodyParser.json({ limit: "50mb" }));

const memoryStore = new session.MemoryStore();

app.use(
    session({
      secret: 'b2bb5de3-d6a1-4967-847a-3d11f30fa913',
      resave: false,
      saveUninitialized: true,
      store: memoryStore,
    })
  );

const keycloak = new Keycloak({ store: memoryStore });

app.use(keycloak.middleware());
  
// Les routes vers vos controllers
app.use("/api", commandeRoutes);
app.use("/api", produitRoutes);
app.use("/api", logRoutes);
// Exportation de app.js pour pouvoir y accéder depuis un autre fichier
module.exports = { app, keycloak };
